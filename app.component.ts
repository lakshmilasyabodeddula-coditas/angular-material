import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/internal/Observable';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'project';
  showSpinner!:true;
  loadData(){
    console.log("data loaded")
  }
  names=0;
  numbers=100;
  myControl = new FormControl()
  options: string[] = ['Angular', 'React', 'Vue']
  objectOptions = [
    { name: 'Angular' },
    { name: 'Angular Material' },
    { name: 'React' },
    { name: 'Vue' }
  ]
  filteredOptions!: Observable<string[]>;

  displayFn(subject: { name: any; }) {
    console.log(subject)
    return subject ? subject.name : undefined
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase()
    return this.options.filter(option =>
      option.toLowerCase().includes(filterValue)
    )
  }
}

